@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Online Users</div>
                <div class="card-body">
                    <ul class="list-group" id="online">
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
