

require('./bootstrap');



import Echo from "laravel-echo"

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

window.Echo.join(`online`)
    .here((users) => {
        users.forEach(function(user) {
            $('#online').append(`<li class="list-group-item" id="user-${user.id}"><span class="fa fa-circle text-success"></span> ${user.name}</li>`);
        })

    })
    .joining((user) => {
        $('#online').append(`<li class="list-group-item" id="user-${user.id}"><span class="fa fa-circle text-success"></span> ${user.name}</li>`);
    })
    .leaving((user) => {
        $("#user-" + user.id).remove();
    });
